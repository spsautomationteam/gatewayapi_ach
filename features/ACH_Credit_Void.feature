@intg
Feature:Verify  ACH credit transaction and void action.


	   
	@ach-credit-void-transaction
	Scenario Outline: Verify  ACH credit transaction and void action.  
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {  "transactionCode": "Credit",   "transactionClass": "<txnclass>",   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And response body path $.message should be ACCEPTED 
	   And I set body to {  "transactionCode": "Void"}
	   When I patch data /ACH/Transactions
       Then response code should be 200
	    Examples:
	    |txnclass|
		|CCD|
		|PPD|
		|WEB|
		#|ARC|
		#|TEL|
		#|RCK|
		
		
	
		
	   

	      
	@ach-credit-transaction-negative
	Scenario Outline: Verify  ACH credit transaction and void action.  
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {  "transactionCode": "Credit",   "transactionClass": "<txnclass>",   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 400
       And response header Content-Type should be application/json
       And response body path $.errorCode should be TransactionRejected       	   	 
	   And response body path $.errorDescription should be CREDIT NOT ALLOWED 	   
	    Examples:
	    |txnclass|		
		|ARC|
		|TEL|
		|RCK|
		
		
	@ach-credit-void-with-invalid-reference-number
	Scenario Outline: Verify  ACH credit transaction and void action for invalid reference-number.  
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {  "transactionCode": "Credit",   "transactionClass": "<txnclass>",   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And response body path $.message should be ACCEPTED 
	   And I set body to {  "transactionCode": "Void"}
	  #When I patch data /ACH/Transactions
	  When I patch data with reference as 123ab for transaction /ACH/Transactions
      Then response code should be 404 
	  
	  Examples:
	  |txnclass|
	  |CCD|
		

	
 
	   