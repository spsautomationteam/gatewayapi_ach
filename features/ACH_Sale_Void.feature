@intg
Feature:Verify  ACH sale transaction with void action.

	   
	@post-transaction-patch-auth-capture   
	Scenario Outline: Verify  ACH sale transaction with void action.	  
	    Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {    "transactionCode": "Sale",   "transactionClass": "<txnclass>" ,   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And response body path $.message should be ACCEPTED 
	   And I set body to {  "transactionCode": "Void"}
	   When I patch data /ACH/Transactions
       Then response code should be 200
    Examples:
	|txnclass|
	|CCD|
	|PPD|
	|WEB|
	|ARC|
	|TEL|
	|RCK|
	   

	
		   
	@post-transaction-patch-auth-capture-amount-zero   
	Scenario Outline: Verify  ACH sale transaction with void action.	  
	    Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {    "transactionCode": "Sale",   "transactionClass": "<txnclass>" ,   "originatorId": "string",   "amounts": {     "total": 0   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 400
	   And response body path $.errorCode should be InvalidRequestData       	   	 
	   And response body path $.errorDescription should be request.Amounts: Invalid Total Amount	   
      
   Examples:
	|txnclass|
	|CCD|
	#|PPD|
	#|WEB|
	#|ARC|
	#|TEL|
	#|RCK|
	
	
	@post-transaction-patch-auth-capture-amount-negative   
	Scenario Outline: Verify  ACH sale transaction with void action with negative amount.	  
	 Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	 And I set content-type header to application/json
	 And I set body to {    "transactionCode": "Sale",   "transactionClass": "<txnclass>" ,   "originatorId": "string",   "amounts": {     "total": -5   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	 When I post data for transaction /ACH/Transactions	 			
     Then response code should be 400     
	 And response body path $.errorCode should be InvalidRequestData       	   	 
	 And response body path $.errorDescription should be request.Amounts: Invalid Total Amount
    Examples:
	|txnclass|
	|CCD|
	#|PPD|
	#|WEB|
	#|ARC|
	#|TEL|
	#|RCK|
	
			   
	@post-transaction-with-empty-routingNumber
	Scenario Outline: Verify  ACH sale transaction with void action.	  
	 Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	 And I set content-type header to application/json
	 And I set body to {    "transactionCode": "Sale",   "transactionClass": "<txnclass>" ,   "originatorId": "string",   "amounts": {     "total": 0   },   "account": {     "type": "Checking",     "routingNumber": "",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	 When I post data for transaction /ACH/Transactions	 			
     Then response code should be 400
	 And response body path $.errorCode should be InvalidRequestData       	   	 
	 And response body path $.errorDescription should be request.Account: The RoutingNumber field is required
    Examples:
	|txnclass|
	|CCD|

	
	@post-transaction-with-empty-accountnumber
	Scenario Outline: Verify  ACH sale transaction with void action.	  
	 Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	 And I set content-type header to application/json
	 And I set body to {    "transactionCode": "Sale",   "transactionClass": "<txnclass>" ,   "originatorId": "string",   "amounts": {     "total": 30  },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": ""   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	 When I post data for transaction /ACH/Transactions	 			
     Then response code should be 400
     And response body path $.errorCode should be InvalidRequestData       	   	 
	 And response body path $.errorDescription should be request.Account: The AccountNumber field is required
    Examples:
	|txnclass|
	|PPD|
	
	
	
	
		
	@post-transaction-with-amount-removed
	Scenario Outline: Verify  ACH sale transaction with void action.	  
	 Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	 And I set content-type header to application/json
	 And I set body to {    "transactionCode": "Sale",   "transactionClass": "<txnclass>" ,   "originatorId": "string",   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	 When I post data for transaction /ACH/Transactions	 			
     Then response code should be 400
     And response body path $.errorCode should be InvalidRequestData       	   	 
	 And response body path $.errorDescription should be request: The Amounts field is required
    Examples:
	|txnclass|
	|WEB|
	#|PPD|
	#|WEB|
	#|ARC|
	#|TEL|
	#|RCK|	
	
   @post-sale-void-with-invalid-reference-number
	Scenario Outline: Verify  ACH sale transaction with void action with invalid reference number.	  
	    Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {    "transactionCode": "Sale",   "transactionClass": "<txnclass>" ,   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And response body path $.message should be ACCEPTED 
	   And I set body to {  "transactionCode": "Void"}	   
	   #When I patch with reference as 123ab for transaction /ACH/Transactions
	   When I patch with reference as 123ab data /ACH/Transactions
       Then response code should be 404
    Examples:
	|txnclass|
	|CCD|
	
    
	 	  
	   