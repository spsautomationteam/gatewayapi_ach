@intg
Feature:Verify  ACH credit by reference transaction


	   
	@ach-sale-credit-by-reference-transaction   
	Scenario Outline: Verify  ACH credit by reference transaction  
	    Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {    "transactionCode": "SALE",   "transactionClass": "<txnclass>",   "originatorId": "string",   "amounts": {     "total": 71   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And response body path $.message should be ACCEPTED 
	   And I set body to {   "amount":70,   "transactionCode": "CreditByReference" }
	   When I post ach with reference data for transaction /ACH/Transactions
       Then response code should be 201
	  Examples:
	  |txnclass|
	  |CCD|
	  |PPD|
	  |WEB|
	  |ARC|
	  |TEL|
	  |RCK|
	  
	  
	 @ach-sale-credit-by-reference-with-invalid-reference-number
	Scenario Outline: Verify  ACH credit by reference transaction  
	    Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {    "transactionCode": "SALE",   "transactionClass": "<txnclass>",   "originatorId": "string",   "amounts": {     "total": 72   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And response body path $.message should be ACCEPTED 
	   And I set body to {   "amount":70,   "transactionCode": "CreditByReference" }
	   #When I post ach with reference data for transaction /ACH/Transactions
	   When I post data with reference as 123ab for transaction /ACH/Transactions
       Then response code should be 400
	  Examples:
	  |txnclass|
	  |CCD|
	  #|PPD|
	  #|WEB|
	  #|ARC|
	  #|TEL|
	  #|RCK|
	   
	  

	   

	
	
    
	 	  
	   