/* jslint node: true */
'use strict';
var apickli = require('apickli');
var factory = require('./factory.js');
var config = require('../../config/config.json');
var Contenttype = "application/json";
var AuthorizationValue;
var SPSUserValue;

var prettyJson = require('prettyjson');


var patchVoidRequestwithReference = function( apickli,referenceval,getTemplateAction,callback ) {	
		
		var pathSuffix =getTemplateAction+"/"+referenceval
		//apickli.scenarioVariables.Reference;		
		var url = apickli.domain +pathSuffix;//+"/"+apickli.scenarioVariables.Reference;			
		apickli.headers['Authorization'] = apickli.scenarioVariables.MidIdValAuthorization
		//apickli.scenarioVariables.Authorization;	
			
		apickli.patch(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\n Cancel RESPONSE :\n"+ response.body);
			}		
			
        	callback();
		});
};

var postTransactionRequestWithReference = function( apickli,referenceval,getTemplateAction,callback ) {
		var pathSuffix =getTemplateAction;
		
		var url = apickli.domain + pathSuffix;
		
		//Math.floor(Math.random() * (Max - min + 1)) + min;
		//var random= Math.floor(Math.random() * (1000)) + 1;
		//console.log("random :"+random)
				
		apickli.addRequestHeader('accept',"application/json");
		apickli.addRequestHeader('Connection', "Keep-Alive");	
		
			apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				
			}			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');							        
        	callback();
		});
};

var postTransactionRequest = function( apickli,getTemplateAction,callback ) {
		var pathSuffix =getTemplateAction;	
		var url = apickli.domain + pathSuffix;		
		//Math.floor(Math.random() * (Max - min + 1)) + min;
		//var random= Math.floor(Math.random() * (1000)) + 1;
		//console.log("random :"+random)				
		apickli.addRequestHeader('accept',"application/json");
		apickli.addRequestHeader('Connection', "Keep-Alive");
		
		
			apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
		
			}
	
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');
			
        	callback();
		});
};


var postAchTransactionRequest = function( apickli,getTemplateAction,callback ) {
		var pathSuffix =getTemplateAction;
		var pathSuffix =getTemplateAction+"/"+apickli.scenarioVariables.Reference;
		var url = apickli.domain + pathSuffix;		
		
		//Math.floor(Math.random() * (Max - min + 1)) + min;
		//var random= Math.floor(Math.random() * (1000)) + 1;
		//console.log("random :"+random)
				
		apickli.addRequestHeader('accept',"application/json");
		apickli.addRequestHeader('Connection', "Keep-Alive");		
		
			apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				
			}
			
			apickli.storeValueOfResponseBodyPathInScenarioScope('$.reference', 'Reference');			
	     // console.log("Reference : "+apickli.scenarioVariables.Reference);			
        	callback();
		});
};





var creditByReferenceTransactionRequest = function( apickli,getTemplateAction,callback ) {
	   		
		var pathSuffix =getTemplateAction+"/"+apickli.scenarioVariables.Reference;			
		var url = apickli.domain +pathSuffix+"/"+apickli.scenarioVariables.Reference;				
		apickli.headers['Authorization'] = apickli.scenarioVariables.Authorization;	
			
		apickli.post(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\n Cancel RESPONSE :\n"+ response.body);
			}		
			
        	callback();
		});
};


var patchVoidRequest = function( apickli,getTemplateAction,callback ) {
			
		var pathSuffix =getTemplateAction+"/"+apickli.scenarioVariables.Reference;	
		
		var url = apickli.domain +pathSuffix;//+"/"+apickli.scenarioVariables.Reference;		
        //console.log("\n\n authorization :"+apickli.scenarioVariables.Authorization)			
		apickli.headers['Authorization'] = apickli.scenarioVariables.MidIdValAuthorization
		//apickli.scenarioVariables.Authorization;				
		apickli.patch(pathSuffix, function (err, response) {
			if ( response ) {
				//console.log("\n Cancel RESPONSE :\n"+ response.body);
			}		
			
        	callback();
		});
};



module.exports = function () { 

		this.Given(/^I have valid Templates request data$/, function (callback) 
		{		
		this.apickli.storeValueInScenarioScope("SPSUserValue", this.apickli.scenarioVariables.SPSUser);
        this.apickli.storeValueInScenarioScope("AuthorizationValue", this.apickli.scenarioVariables.Authorization);			
		callback();
		
	});
	
	this.When(/^I get data for Templates (.*)$/, function (getTemplateAction,callback) {
	      	getTemplateData(this.apickli,getTemplateAction,callback);
	});	
	
	
	
	this.When(/^I post data for transaction (.*)$/, function (getTemplateAction,callback) {		   
	      	postTransactionRequest(this.apickli,getTemplateAction,callback);
	});	
	
	    
	this.When(/^I post data with reference as (.*) for transaction (.*)$/, function (referenceval,getTemplateAction,callback) {		   
	      	postTransactionRequestWithReference(this.apickli,referenceval,getTemplateAction,callback);
	});
	
	
	this.When(/^I post ach with reference data for transaction (.*)$/, function (getTemplateAction,callback) {		   
	      	postAchTransactionRequest(this.apickli,getTemplateAction,callback);
	});
		
	this.When(/^I post data for credit by reference transaction (.*)$/, function (getTemplateAction,callback) {
	      	creditByReferenceTransactionRequest(this.apickli,getTemplateAction,callback);
	});	
	
	
	
	this.When(/^I patch data (.*)$/, function (getTemplateAction,callback) {
	      	patchVoidRequest(this.apickli,getTemplateAction,callback);
	});	
	
		this.When(/^I patch with reference as (.*) data (.*)$/, function (referenceval,getTemplateAction,callback) {
	      	patchVoidRequestwithReference(this.apickli,referenceval,getTemplateAction,callback);
	});		
	
	
	this.Given(/^I enter invalid SPS-User request data$/, function (callback) {	
		this.apickli.storeValueInScenarioScope("SPSUserValue", this.apickli.scenarioVariables.InvalidSPSUser);
        this.apickli.storeValueInScenarioScope("AuthorizationValue",this.apickli.scenarioVariables.Authorization);
	
		callback();
		
	});
	
	this.Given(/^I enter invalid Authorization request data$/, function (callback) {		
		this.apickli.storeValueInScenarioScope("SPSUserValue", this.apickli.scenarioVariables.SPSUser);
        this.apickli.storeValueInScenarioScope("AuthorizationValue", this.apickli.scenarioVariables.InvalidAuthorization);	
		callback();
		
	});
	
	
	
};

var prettyPrintJson = function(json) {
    var output = {
        stepContext: stepContext,
        testOutput: json
    };
    
    return prettyJson.render(output, {
        noColor: true
    });
};
