/* jslint node: true */
'use strict';

var factory = require('./factory.js');
//var hmacTools = new (require("../../../hmacTools.js"))();
var hmacTools = require("./hmacTools.js");

var createMerchant = function( apickli, type, callback ) {
		var pathSuffix = "/merchants";
		var url = apickli.domain + pathSuffix;
		var reqString = 
            type.toLowerCase() == "bankcard" ?  { cardData: { number: "5454545454545454", expiration: "1219" } } :
            type.toLowerCase() == "ach" ?  { account: { type: "Checking", routingNumber: "056008849", accountNumber: "12345678901234" } } :
            "";
        
		var body = JSON.stringify(reqString);

		apickli.setRequestBody(body);
        var nonce = hmacTools.nonce(12);
        var timestamp = Date.now() / 1000 + '';

		var clientSecret = apickli.scenarioVariables.clientSecret;
		var hmac = hmacTools.hmac(clientSecret, "POST", url, body, '', nonce, timestamp);
        apickli.addRequestHeader('clientId', apickli.scenarioVariables.clientId);
        apickli.addRequestHeader('nonce', nonce);
        apickli.addRequestHeader('timestamp', timestamp);
        apickli.addRequestHeader('Authorization', hmac);
        apickli.addRequestHeader('content-type', 'application/json');

		apickli.post(pathSuffix, function (err, response) {
        	apickli.storeValueOfResponseBodyPathInScenarioScope('$.vaultResponse.status', 'vaultStatus');
            apickli.storeValueOfResponseBodyPathInScenarioScope('$.vaultResponse.data', 'vaultData');
        	callback();
		});
};

module.exports = function () { 
	//this.Given(/^I have valid merchant credentials$/, function (callback) {
	//    // this.apickli.storeValueInScenarioScope("merchantId", "887681827469");
	//    // this.apickli.storeValueInScenarioScope("merchantKey", "T1E3O2L2H8P1");
	//	callback();
	//});

	this.Given(/^I create a (.*) merchant/, function (type, callback) {
		createMerchant(this.apickli, type, callback);
	});

	//this.When(/^I capture that charge with a (.*) dollar amount$/, function (amount, callback) {
    //    var authorizationReference = this.apickli.scenarioVariables.authorizationReference;
	//	// console.log("REFERENCE: ", authorizationReference );
	//	captureAuthorization(this.apickli, authorizationReference, amount, callback);
	//});

	//this.When(/^I get that same charge$/, function (callback) {
	//    var authorizationReference = this.apickli.scenarioVariables.authorizationReference;
	//    // console.log("REFERENCE: ", authorizationReference );
	//    getCharge(this.apickli, authorizationReference, callback);
	//});
    
};
