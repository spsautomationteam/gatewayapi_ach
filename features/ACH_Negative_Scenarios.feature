@intg
Feature:Verify  negative ACH  transactions
	   
	@ach-wrong-json-data-transaction 
	Scenario: Verify  ACH sale transaction with void action.	  
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to ab123
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 400            	   	 
	   And response body path $.message should be No request content was found 	
    
	@ach-wrong-empty-transaction-code-transaction
	Scenario Outline: Verify  the Sale transaction with  wrong transactionCode.  
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {    "transactionCode": "<txncode>",   "transactionClass": "CCD" ,   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 400       
       And response body path $.errorCode should be InvalidRequestData       	   	 
	   And response body path $.errorDescription should be <errDescription>
	   Examples:
	  |txncode|errDescription|	   
	  |123|request: This route can be used only for Authorization, Sale, or Credit|
	  |   |request: The TransactionCode field is required|
    
	@ach-transaction-code-field-removed
	Scenario: Verify  the Sale transaction with  transaction code field removed
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {     "transactionClass": "CCD" ,   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 400       
       And response body path $.errorCode should be InvalidRequestData       	   	 
	   And response body path $.errorDescription should be request: The TransactionCode field is required
	
	@ach-transaction-class-field-wrong
	Scenario: Verify  the Sale transaction with  wrong transaction class.  
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {  "transactionCode": "CCD",   "transactionClass": "123" ,   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 400       
       And response body path $.errorCode should be InvalidRequestData       	   	 
	   And response body path $.errorDescription should be request: The TransactionCode field is required.; Error converting value 

	@ach-transaction-class-field-removed
	Scenario: Verify  the Sale transaction with   transaction class removed.  
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {  "transactionCode": "Credit",      "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 400       
       And response body path $.errorCode should be InvalidRequestData       	   	 
	   And response body path $.errorDescription should be request: SecCode is required   	   

  
	@ach-empty-json-data
	Scenario: Verify  the Sale transaction with  wrong transactionCode.  
	   Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {}
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 400       
       And response body path $.errorCode should be InvalidRequestData       	   	 
	   And response body path $.errorDescription should be request: The TransactionCode field is required.; request: The Amounts field is required.; request: The Billing field is required.
	   
	
	   
	@ach-sale-transaction-wrong-authorization-code
	Scenario Outline: Verify  ACH credit transaction and void action.  
	   Given I set Authorization header with MID as 999999999997 and MKEY as X3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {  "transactionCode": "Sale",   "transactionClass": "<txnclass>",   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 401       
       And response body path $.errorCode should be InvalidCredentials       	   	 
	   And response body path $.errorDescription should be The client credentials supplied are invalid
	Examples:
	|txnclass|
    |CCD|
	
	@ach-sale-transaction-invalid-authorization-code
	Scenario Outline: Verify  ACH credit transaction and void action.  
	   #Given I set Authorization header with MID as 999999999997 and MKEY as X3QD6YWYHFD
	   Given I set Authorization header to xyz
	   And I set content-type header to application/json
	   And I set body to {  "transactionCode": "Sale",   "transactionClass": "<txnclass>",   "originatorId": "string",   "amounts": {     "total": 65   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 401       
       And response body path $.errorCode should be InvalidHeaders       	   	 
	   And response body path $.errorDescription should be Required Authorization header not present
	   Examples:
	   |txnclass|
	   |PPD|
		
	@ach-credit-credit-by-reference-transaction   
	Scenario Outline: Verify  ACH credit by reference transaction  for a CREDIT transaction
	    Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {    "transactionCode": "CREDIT",   "transactionClass": "<txnclass>",   "originatorId": "string",   "amounts": {     "total": 71   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And response body path $.message should be ACCEPTED 
	   And I set body to {   "amount":70,   "transactionCode": "CreditByReference" }
	   When I post ach with reference data for transaction /ACH/Transactions
       Then response code should be 201
	  Examples:
	  |txnclass|
	  |CCD|
	 #|PPD|
	 #|WEB|
	 #|ARC|
	 #|TEL|
	 #|RCK|
	   
	   
	@ach-sale-credit-by-reference-with-invalid-reference-number
	Scenario Outline: Verify  ACH credit by reference with invalid reference nunber 
	    Given I set Authorization header with MID as 999999999997 and MKEY as K3QD6YWYHFD
	   And I set content-type header to application/json
	   And I set body to {    "transactionCode": "SALE",   "transactionClass": "<txnclass>",   "originatorId": "string",   "amounts": {     "total": 71   },   "account": {     "type": "Checking",     "routingNumber": "123123123",     "accountNumber": "1234567890"   },  "customer": {     "dateOfBirth": "2016-01-29T05:49:05.703Z",     "ssn": "123456789",     "license": {       "number": "string",       "stateCode": "VA"     },     "ein": "test",     "email": "achtest@gmail.com",     "telephone": "1458693352",     "fax": "1458965866"   },     "billing": {     "name": {       "first": "test",       "middle": "test",       "last": "test",       "suffix": "t"     },     "city": "roanoke",     "state": "virginia",     "country": "unitedstates",     "address": "test",     "postalCode": "24011"   },     }
	   When I post data for transaction /ACH/Transactions	 			
       Then response code should be 201
       And response header Content-Type should be application/json
       And response body path $.status should be Approved       	   	 
	   And response body path $.message should be ACCEPTED 
	   And I set body to {   "amount":70,   "transactionCode": "CreditByReference" }
	   When I post ach with reference data for transaction /ACH/Transactions
       Then response code should be 201
	  Examples:
	  |txnclass|
	  |CCD|
	  |PPD|
	  |WEB|
	  |ARC|
	  |TEL|
	  |RCK|	   
